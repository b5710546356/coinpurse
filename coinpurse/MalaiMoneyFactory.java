package coinpurse;

import java.util.Arrays;
import java.util.List;

public class MalaiMoneyFactory extends MoneyFactory{

	Valuable createMoney(double value) {
		List<Double> listCoins = Arrays.asList(0.05, 0.10, 0.20, 0.50);
		List<Double> listBanks = Arrays.asList(1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0);
		
		if( listCoins.contains(value) ){
			return new Coin(value*100,"Sen");
		}
		else if( listBanks.contains(value) ){
			return new BankNote(value,"Ringgit");
		}
		throw new IllegalArgumentException();
	}
	
}
