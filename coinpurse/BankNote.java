package coinpurse;

/**
 * A banknote with a monetary value.
 * You can't change the value of a banknote.
 * @author Phasin Sarunpornkul
 */
public class BankNote extends AbstractValuable{
	
	 /** Value of the coin */
	private double value;
	 /** id of the banknote */
	private int serialNumber;
	/** id of the next banknote */
	private static int nextSerialNumber = 1000000;
	private String currency;
	
	/** 
     * Constructor for a new banknote. 
     * @param value is the value for the banknote.
     */
	public BankNote (double value, String currency){
		this.value = value;
		this.currency = currency;
		this.serialNumber = this.nextSerialNumber;
		this.nextSerialNumber++;
	}

	/**
     * to get the value of this banknote.
     * @return value of this banknote.
     */
	public double getValue() {
		return this.value;
	}

	/**
     * to get the next id of this banknote.
     * @return next id of this banknote.
     */
	public int getNextSerialNumber(){
		return this.nextSerialNumber;
	}
	
	 /**
     * to print the value and serial number of this banknote.
     * @return return a string description of banknote value with Baht and serial number of this banknote in bracket. 
     */
	public String toString() {
		return String.format("%.0f-%s Banknote [%d]", this.value, this.currency, this.serialNumber);
	}
	
}
