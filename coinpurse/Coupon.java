package coinpurse;

import java.util.HashMap;

import java.util.Map;

/**
 * A coupon with a monetary value.
 * You can't change the value of a coupon.
 * @author Phasin Sarunpornkul
 */
public class Coupon extends AbstractValuable{
	
	/** color type of the coupon */
	private String color;
	/** to stock the value with color type */
	Map <String, Double> map = new HashMap();
	
	/** 
     * Constructor for a new coupon. 
     * @param color is the color type of the coupon.
     */
	public Coupon (String color){
		color =color.toUpperCase();
		this.color = color.charAt(0)+color.substring(1,color.length()).toLowerCase();
		map.put("Red", 100.0);
		map.put("Blue", 50.0);
		map.put("Green", 20.0); 
	}

	/**
     * to get the value of this coupon.
     * @return value of this coupon.
     */
	public double getValue() {
		return map.get(this.color);
	}
	
	public String toString() {
		return this.color+" coupon";
	}
	
}
