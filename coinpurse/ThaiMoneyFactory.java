package coinpurse;

import java.util.Arrays;
import java.util.List;

public class ThaiMoneyFactory extends MoneyFactory{

	Valuable createMoney(double value) {
		List<Double> listCoins = Arrays.asList(1.0, 2.0, 5.0, 10.0);
		List<Double> listBanks = Arrays.asList(20.0, 50.0, 100.0, 500.0, 1000.0);
		
		if( listCoins.contains(value) ){
			return new Coin(value,"Baht");
		}
		else if( listBanks.contains(value) ){
			return new BankNote(value,"Baht");
		}
		throw new IllegalArgumentException();
	}

}
