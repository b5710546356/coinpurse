package coinpurse;
/**
 * AbstractValuable is abstract class that contain compareTo and equals method.
 * @author Phasin Sarunpornkul
 *
 */
public abstract class AbstractValuable implements Valuable {
	 
    /**
     * to compare the value of other Valuable more or less than value of this Valuable.
     * @param value is the Valuable class to compare the value.
     * @return result of comparison between 2 Valuable be integer .
     */
	public int compareTo(Valuable value){
		return (this.getValue()+"").compareTo(value.getValue()+"");
	}
	
	/**
     * to check value of other Valuable equal value of this Valuable?
     * @param obj is the object to check equal value.
     * @return return true when other Valuable equal this Valuable.
     */
	public boolean equals (Object obj){
		if(obj!=null&&(obj.getClass()==this.getClass())&&((AbstractValuable)obj).getValue()==this.getValue()){
    		return true;
    	}
		return false;
	}	
}
