package coinpurse;

import java.util.ResourceBundle;

public abstract class MoneyFactory {

	private static MoneyFactory instance= null;

	protected MoneyFactory(){
		instance = this;
	}

	public static MoneyFactory getInstance(){
		if(instance==null){
			setMoneyFactory();
		}
		return instance;
	}

	public static void setMoneyFactory(){
		ResourceBundle bundle = ResourceBundle.getBundle( "purse" );
		String factoryclass = bundle.getString("moneyfactory");
		try {
			instance = (MoneyFactory)Class.forName(factoryclass).newInstance();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			instance = new MalaiMoneyFactory();
		}
	}

	abstract Valuable createMoney(double value);

	public Valuable createMoney(String value){
		return createMoney(Double.parseDouble(value));
	}
}
