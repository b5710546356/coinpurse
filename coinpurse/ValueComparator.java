package coinpurse;
import java.util.Comparator;
/**
 * A ValueComparator used for compare 2 value to sort.
 * @author Phasin Sarunpornkul
 *
 */
public class ValueComparator implements Comparator<Valuable>{
	
	/**
	 * to compare the value between 2 Valuable.
	 * @param 2 Value of money.
	 * @return result of comparison between 2 valuable be integer.
	 */
	public int compare(Valuable a, Valuable b) {
    	if (a.getValue() < b.getValue())
            return 1;

        if (a.getValue() == b.getValue())
        {
            return 0;
        }

        return -1;
    
    }

}
