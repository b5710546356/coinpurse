package coinpurse;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;

/**
 * Observe the purse.
 * @author Phasin Sarunpornkul
 *
 */
public class PurseObserver implements Observer {
	
	/**
	 * Constructor new PurseObserver
	 */
	public PurseObserver(){
		
	}
	
	/**
	 * update receives notification from the purse
	 * @param subject
	 * @param info
	 */
	public void update(Observable subject, Object info){
		if (subject instanceof Purse){
			Purse purse = (Purse)subject;
			int balance = (int)purse.getBalance();
			System.out.println("Balance is: "+balance);
		}
		if(info != null) System.out.println(info);
	}
}
