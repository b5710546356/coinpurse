package coinpurse;

import java.awt.FlowLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

/**
 * Graphic user interface to show the balance value in purse.
 * @author Phasin Sarunpornkul
 *
 */
public class PurseBalanceObserver extends JFrame implements Observer {
	/*Panel of this frame*/
	private JPanel contentPane;
	/*Label to show the balance value of this purse*/
	private JLabel labelbalance;
	
	/**
	 * Constructor new PurseBalanceObserver.
	 */
	public PurseBalanceObserver(){
		setTitle("Purse Balance");
		setBounds(180, 100, 800, 600);
		super.setResizable(false);
		super.setSize(250, 59);
		this.initComponets();
	}
	
	/**
	 * initial componets for this GUI.
	 */
	public void initComponets(){
		contentPane = new JPanel();
		contentPane.setLayout(new FlowLayout());
		labelbalance = new JLabel("0 Baht",JLabel.CENTER);
		contentPane.add(labelbalance);
		super.add(contentPane);
	}
	
	/**
	 * to run this GUI.
	 */
	public void run(){
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	/**
	 * to update the label to show the balance value of this purse.
	 */
	public void update(Observable subject, Object info){
		if (subject instanceof Purse){
			Purse purse = (Purse)subject;
			labelbalance.setText((int)purse.getBalance()+" Baht");
		}
		if(info != null) System.out.println(info);
	}
	
	
}
