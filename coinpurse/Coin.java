package coinpurse;
 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Phasin Sarunpornkul
 */
public class Coin extends AbstractValuable {

    /** Value of the coin */
    private double value;
    private String currency;
    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin.
     */
    public Coin( double value, String currency ) {
        this.value = value;
        this.currency = currency;
    }


    /**
     * to get the value of this coin.
     * @return value of this coin.
     */
    public double getValue(){
    	return this.value;
    }
    
    /**
     * to print the value of this coin.
     * @return return a string description of coin value with Baht. 
     */
    public String toString(){
    	return String.format("%.0f-%s coin", this.value, this.currency);
    }
}
