package coinpurse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;

/**
 *  A coin purse contains money.
 *  You can insert money, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  money to remove.
 *  
 *  @author Phasin Sarunpornkul
 */
public class Purse extends Observable  {
    /** Collection of money in the purse. */
	private List<Valuable> money;
    
    /** Capacity is maximum NUMBER of money the purse can hold.
     *  Capacity is set when the purse is created.
     */
    private int capacity;
    
    /**
     * withdraw with differ strategies.
     */
    private WithdrawStrategy strategy;
    
    /** 
     *  Create a purse with a specified capacity.
     *  @param capacity is maximum number of money you can put in purse.
     */
    public Purse( int capacity ) {
    	money = new ArrayList<Valuable>();
    	if(capacity<=0) {
    	      throw new IllegalArgumentException("the capacity must more than zero");
    	 }
    	this.capacity = capacity;
    }

    /**
     * Count and return the number of money in the purse.
     * This is the number of money, not their value.
     * @return the number of money in the purse
     */
    public int count() { return this.money.size(); }
    
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    public double getBalance() {
    	double balance =0;
    	for(int i=0;i<this.money.size();i++){
    		balance += this.money.get(i).getValue();
    	}
    	return balance; 
    }

    
    /**
     * Return the capacity of the purse.
     * @return the capacity
     */
    public int getCapacity() { return this.capacity; }
    
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    public boolean isFull() {
    	if(this.money.size()==this.capacity){
    		return true;
    	}
        return false;
    }

    /** 
     * Insert a money into the purse.
     * The money is only inserted if the purse has space for it
     * and the money has positive value.  No worthless coins!
     * @param money is a Valuable object to insert into purse
     * @return true if money inserted, false if can't insert
     */
    public boolean insert( Valuable money ) {
        // if the purse is already full then can't insert anything.
    	if(this.isFull()==false&&money.getValue()>0){
    		this.money.add(money);
    		super.setChanged();
    		super.notifyObservers(this);
    		return true;
    	}
    	
        return false;
    }
    
    
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of money withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */    
    public Valuable[] withdraw(double amount){
    	if(this.strategy ==null){
    		throw new IllegalArgumentException("You must set strategy first");
    	}
    	Valuable[] moneylist = this.strategy.withdraw(amount, money);
    	super.setChanged();
		super.notifyObservers(this);
    	return moneylist;
    }
    
    /**
     * to set new strategy type.
     * @param strategy type that want to set.
     */
    public void setWithdrawStrategy(WithdrawStrategy strategy){
    	this.strategy = strategy;
    }
    /** 
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     */
    public String toString() {
       
    	return this.money.size()+" money with value "+this.getBalance();
    }

}

