package coinpurse;

public class Test {
	public static void main(String[] args) {
		
		/*
		MoneyFactory factory = MoneyFactory.getInstance();
		Valuable m = factory.createMoney( 5.0 );
		System.out.println(m.toString());
		// "5 Baht coin"
		Valuable m2 = factory.createMoney("1000.0");
		System.out.println(m2.toString()); 
		// "1000 Baht Banknote"
		Valuable m3 = factory.createMoney(0.05);
		// throws IllegalArgumentException
		*/
		
		
		MoneyFactory factory = MoneyFactory.getInstance();
		Valuable m = factory.createMoney( 5 );
		 System.out.println(m.toString()); 
		  //"5 Ringgit Banknote"
		// Valuable m2 = factory.createMoney("1000.0");
		 //throws IllegalArgumentException
		 Valuable m3 = factory.createMoney( 0.05 );
		 System.out.println(m3.toString());  
		  //"5 Sen coin"
		 
	}
}
