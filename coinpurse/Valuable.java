package coinpurse;
/**
 * Valuable is interface which contain getValue method. 
 * 
 * @author Phasin Sarunpornkul
 *
 */
public interface Valuable extends Comparable<Valuable>{

	/**
	 * getValue is method that return the value.
	 * @return the value that type double.
	 */
	public double getValue();
	
}
