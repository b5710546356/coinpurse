package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;

/**
 * 
 * Withdraw the valuable with recursion algorithm.
 * @author Phasin Sarunpornkul
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy{

	 /**  
     *  Withdraw the requested amount of money.
     *  Return an array of money withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @param list of available valuables.
     *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
	public Valuable[] withdraw(double amount, List<Valuable> list){
		double balance =0;
		for(int i=0;i<list.size();i++){
			balance += list.get(i).getValue();
		}
		if(amount<0){
			return null;
		}
		else if(amount>balance){
			return null;
		}
		
		List<Valuable> withdrawlist = new ArrayList<Valuable>();
		withdrawlist = withdrawFrom(amount,list,list.size()-1);
		if(withdrawlist!=null){
		Valuable withdrawmoney []= new Valuable [withdrawlist.size()];
		for(int i=0;i<withdrawmoney.length;i++){
			withdrawmoney[i] = (Valuable)withdrawlist.get(i);
		}
		for(int i=0;i<withdrawlist.size();i++){
			for(int j=0;j<list.size();j++){
				if(withdrawlist.get(i)==list.get(j)){
					list.remove(j);
				}
			}
		}
		return withdrawmoney;
		}
		return null;
	}

	/**
	 * a helper method for recursion 
	 * @param amount is the amount to withdraw.
	 * @param list of available valuables.
	 * @param lastindex of the list.
	 * @return return list that can withdraw or null if can't withdraw
	 */
	public List<Valuable> withdrawFrom(double amount, List<Valuable>list,int lastindex){
		if(amount<0){
			return null;
		}
		if(lastindex<0){
			return null;
		}
		if(amount-list.get(lastindex).getValue()==0){
			List<Valuable> withdraw = new ArrayList<Valuable>();
			withdraw.add(list.get(lastindex));
			return withdraw;
		}
		List<Valuable> withdraw = withdrawFrom(amount-list.get(lastindex).getValue(),list,lastindex-1);
		if(withdraw==null){
			withdraw = withdrawFrom(amount,list,lastindex-1);
		}
		else{
			withdraw.add(list.get(lastindex));
		}
		return withdraw;
	}

}
