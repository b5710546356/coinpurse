package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;

/*
 * WithdrawStrategy is interface which contain withdraw method for 
 * withdraw money from Purse. 
 * @ author Phasin Sarunpornkul
 */

public interface WithdrawStrategy {
	/*
	 * withdraw money from Purse. 
	 * @param amount of the valuables that want to withdraw.
	 * @param list of available valuables.
	 * @return  array of remaining value after withdraw.
	 */
	public Valuable[] withdraw(double amount,List<Valuable> valuables);
	
	
}
