package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Coin;
import coinpurse.Valuable;
import coinpurse.ValueComparator;

/**
 * 
 * Withdraw the valuable with greedy algorithm.
 * @author Phasin Sarunpornkul
 *
 */
public class GreedyWithdraw implements WithdrawStrategy{
	 /**  
     *  Withdraw the requested amount of money.
     *  Return an array of money withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @param list of available valuables.
     *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
	public Valuable[] withdraw( double amount,List<Valuable> valuables ) {
        
    	if(amount<0){
    		return null;
    	}
        
	   /*
		* One solution is to start from the most valuable money
		* in the purse and take any money that maybe used for
		* withdraw.
		* Since you don't know if withdraw is going to succeed, 
		* don't actually withdraw the money from the purse yet.
		* Instead, create a temporary list.
		* Each time you see a money that you want to withdraw,
		* add it to the temporary list and deduct the value
		* from amount. (This is called a "Greedy Algorithm".)
		* Or, if you don't like changing the amount parameter,
		* use a local total to keep track of amount withdrawn so far.
		* 
		* If amount is reduced to zero (or tempTotal == amount), 
		* then you are done.
		* Now you can withdraw the money from the purse.
		* NOTE: Don't use list.removeAll(templist) for this
		* becuase removeAll removes *all* money from list that
		* are equal (using Valuable.equals) to something in templist.
		* Instead, use a loop over templist
		* and remove money one-by-one.		
		*/
		
		// Did we get the full amount?
		if ( amount > 0 )
		{	// failed. Since you haven't actually remove
			// any money from Purse yet, there is nothing
			// to put back.
			double balance =0;
	    	for(int i=0;i<valuables.size();i++){
	    		balance += valuables.get(i).getValue();
	    	}
	    	
			if(amount>balance){
				return null;
			}
			else{
				Collections.sort( valuables, new ValueComparator() );
				List withdrawmoney = new ArrayList<Coin>();
				List withdrawindex = new ArrayList<Integer>();
				double value=amount;
				for(int i=0;i<valuables.size();i++){
					if(value>=valuables.get(i).getValue()){
						withdrawmoney.add(valuables.get(i));
						withdrawindex.add(i);
						value-=valuables.get(i).getValue();
					}

				}
				if(value==0){
					Valuable stockmoney []= new Valuable [withdrawmoney.size()];
					for(int i=0;i<stockmoney.length;i++){
						stockmoney[i] = (Valuable)withdrawmoney.get(i);
					}
					for(int i=withdrawindex.size()-1;i>=0;i--){
						valuables.remove((int)withdrawindex.get(i));
					}
					return stockmoney;
				}
				else{
					return null;
				}
				
			}
			
		}

		// Success.
		// Since this method returns an array of money,
		// create an array of the correct size and copy
		// the money to withdraw into the array.
        
        return null;
	}
}
