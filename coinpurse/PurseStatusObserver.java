package coinpurse;

import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

/**
 * Graphic user interface to show the status of item in purse.
 * @author Phasin Sarunpornkul
 *
 */

public class PurseStatusObserver extends JFrame implements Observer{
	/*Panel of this frame*/
	private JPanel contentPane;
	/*Label to show how many item of this purse*/
	private JLabel labelitem; 
	/*Progressbar to show bar by percent of item*/
	private JProgressBar progressbar;
	
	/**
	 * Constructor new PurseStatusObserver.
	 */
	public PurseStatusObserver(){
		setTitle("Purse Status");
		setBounds(180, 200, 800, 600);
		super.setResizable(false);
		super.setSize(250, 59);
		this.initComponets();
	}
	
	/**
	 * initial componets for this GUI.
	 */
	public void initComponets(){
		contentPane = new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane,BoxLayout.Y_AXIS));
		labelitem = new JLabel("Empty");
		progressbar = new JProgressBar();
		contentPane.add(labelitem);
		contentPane.add(progressbar);
		super.add(contentPane);
	}
	
	/**
	 * to run this GUI.
	 */
	public void run(){
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	/**
	 * to update the status of this purse.
	 */
	public void update(Observable subject, Object info){
		if (subject instanceof Purse){
			Purse purse = (Purse)subject;
			progressbar.setMaximum(purse.getCapacity());
			progressbar.setMinimum(0);
			progressbar.setValue(purse.count());
			if(purse.isFull()==true){
				labelitem.setText("Full");
			}
			else if(purse.count()==0){
				labelitem.setText("Empty");
			}
			else{
				labelitem.setText(purse.count()+" items");
			}
		}
		if(info != null) System.out.println(info);
	}

}
