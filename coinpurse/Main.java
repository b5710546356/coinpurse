package coinpurse;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 */
public class Main {

    /**
     * @param args not used
     */
    public static void main( String[] args ) {
    	Purse purse = new Purse(10);
    	purse.setWithdrawStrategy(new RecursiveWithdraw());
    	PurseBalanceObserver purseBalance = new PurseBalanceObserver();
    	purseBalance.run();
    	purse.addObserver(purseBalance);
    	PurseStatusObserver purseStatus = new PurseStatusObserver();
    	purseStatus.run();
    	purse.addObserver(purseStatus);
    	ConsoleDialog console = new ConsoleDialog(purse);
    	console.run();
    	
    }
}
